

	//marder init

	_veh = _this select 0;

	waitUntil {!isNull _veh};

	[_veh] spawn redd_fnc_main_plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_marder_zug;//spawns function to set platoon letter and vehicle number
	[_veh] spawn redd_fnc_marder_bat_Komp;//spawns function to set battalion and company numbers
	
	_veh setVariable ['Redd_Marder_Commander_Up', false,true];//initiates varibale for "climp up" and "climp down" function of turned out commander
	_veh setVariable ['Redd_Marder_Commander_Winkelspiegel', false,true];//initiates varibale for "periskope mirror" function of commander
	
	[_veh,[[1],true]] remoteExecCall ["lockTurret"]; //locks Milan