	
	
	//Basic parameters
	simulation = tankX;
	dampersBumpCoef = 0.3;
	terrainCoef = 0;

	//Fuel
	#define FUEL_FACTOR 0.165
	fuelCapacity = 650 * FUEL_FACTOR;
    ACE_refuel_fuelCapacity = 650;

	//Differential parameters
	//Nix

	//Engine parameters
	maxOmega = 261.8;
	minOmega = 83.78;
	enginePower = 447;
	peakTorque = 2350;
	engineMOI = 9;
	idleRpm = 800;
	redRpm = 2500;
	clutchStrength = 153;
	dampingRateFullThrottle = 0.25;
	dampingRateZeroThrottleClutchEngaged = 8;
	dampingRateZeroThrottleClutchDisengaged = 0.25;
	maxSpeed = 65; 
	thrustDelay	= 0.05;
	brakeIdleSpeed = 2.8;
	normalSpeedForwardCoef = 0.49;
	slowSpeedForwardCoef = 0.33;//0.25

	tankTurnForce = 11*37500/2;
	tankTurnForceAngMinSpd = 1;
	tankTurnForceAngSpd = 1;

	accelAidForceCoef = 4;
	accelAidForceYOffset = -1.5;
	accelAidForceSpd = 13;
	
	torqueCurve[] = 
	{

		{"(0/2500)","(0/2350)"},
		{"(357/2500)","(1900/2350)"},
		{"(714/2500)","(2100/2350)"},
		{"(1071/2500)","(2250/2350)"},
		{"(1428/2500)","(2350/2350)"},
		{"(1785/2500)","(2350/2350)"},
		{"(2142/2500)","(1965/2350)"},
		{"(2500/2500)","(0/2350)"}

	};

	//Floating and sinking
	waterPPInVehicle = 0;
	maxFordingDepth = -0.75;
	waterResistance = 0;
	canFloat = 0;
	waterLeakiness = 10;
	
	//Anti-roll bars
	antiRollbarForceCoef = 85;
	antiRollbarForceLimit = 85;	
	antiRollbarSpeedMin = 0;
	antiRollbarSpeedMax	= 65;

	//Gearbox
	class complexGearbox 
	{
		
		GearboxRatios[] = {"R1", -2.3, "N", 0, "D1", 7, "D2", 4, "D3", 3, "D4", 2.31};
		TransmissionRatios[] = {"High",6};
		gearBoxMode        = "auto";
		moveOffGear        = 1;
		driveString        = "D";
		neutralString      = "N";
		reverseString      = "R";

	};
	
	changeGearType="rpmratio";
	
	changeGearOmegaRatios[]=
	{

		1.0,0.45,
		0.6,0.45,
		0.9,0.45,
		0.9,0.45,
		0.9,0.45,
		1.0,0.6
		
	};

	switchTime = 0;
	latency = 1.5;
	engineLosses = 25;
	transmissionLosses = 15;

	//Wheel parameters
	driveOnComponent[] = {};
	wheelCircumference = 2.2;
	numberPhysicalWheels = 16;
	turnCoef = 5;

	class Wheels
	{
		class L2
		{
			
			boneName = "wheel_podkoloL1";
			center   = "wheel_1_2_axis";
			boundary = "wheel_1_2_bound";
			damping = 75;
			steering = 0;
			side = "left";
			mass = 150;
			width = 0.52;
			MOI = 9;
			latStiffX = 25;
			latStiffY = 180;
			longitudinalStiffnessPerUnitGravity = 100000;
			maxBrakeTorque = 37500;
			sprungMass = 3125;
			springStrength = 264063;
			springDamperRate = 17236;
			dampingRate = 1;
			dampingRateInAir = 1300;
			dampingRateDamaged = 10.0;
			dampingRateDestroyed = 10000;
			maxCompression = 0.15;
			maxDroop = 0.15;
			frictionVsSlipGraph[]={	{0,1},{0.35,0.95},{1,0.5}};

		};
		class L3: L2
		{
			
			boneName="wheel_podkolol2";
			center="wheel_1_3_axis";
			boundary="wheel_1_3_bound";
			
		};
		
		class L4: L2
		{
			
			boneName="wheel_podkolol3";
			center="wheel_1_4_axis";
			boundary="wheel_1_4_bound";
			
		};
		
		class L5: L2
		{
			
			boneName="wheel_podkolol4";
			center="wheel_1_5_axis";
			boundary="wheel_1_5_bound";
			
		};
		
		class L6: L2
		{
			
			boneName="wheel_podkolol5";
			center="wheel_1_6_axis";
			boundary="wheel_1_6_bound";
			
		};
		
		class L7: L2
		{
			
			boneName="wheel_podkolol6";
			center="wheel_1_7_axis";
			boundary="wheel_1_7_bound";
			
		};

		//Triebrad
		class L1: L2
		{
			
			boneName="";
			center="wheel_1_1_axis";
			boundary="wheel_1_1_bound";
			maxDroop=0;
			maxCompression=0;

		};

		//Umlenkrolle
		class L8: L2
		{
			
			boneName="";
			center="wheel_1_8_axis";
			boundary="wheel_1_8_bound";
			maxDroop=0;
			maxCompression=0;
			
		};
		
		class R2: L2
		{
			
			side="right";
			boneName="wheel_podkolop1";
			center="wheel_2_2_axis";
			boundary="wheel_2_2_bound";
			
		};
		
		class R3: R2
		{
			
			boneName="wheel_podkolop2";
			center="wheel_2_3_axis";
			boundary="wheel_2_3_bound";
			
		};
		class R4: R2
		{
			
			boneName="wheel_podkolop3";
			center="wheel_2_4_axis";
			boundary="wheel_2_4_bound";
			
		};
		
		class R5: R2
		{
			
			boneName="wheel_podkolop4";
			center="wheel_2_5_axis";
			boundary="wheel_2_5_bound";
			
		};
		
		class R6: R2
		{
			
			boneName="wheel_podkolop5";
			center="wheel_2_6_axis";
			boundary="wheel_2_6_bound";
			
		};
		
		class R7: R2
		{
			
			boneName="wheel_podkolop6";
			center="wheel_2_7_axis";
			boundary="wheel_2_7_bound";
			
		};

		//Triebrad
		class R1: R2
		{
			
			boneName="";
			center="wheel_2_1_axis";
			boundary="wheel_2_1_bound";
			maxDroop=0;
			maxCompression=0;
			
		};

		//Umlenkrolle
		class R8: R2
		{
			
			boneName="";
			center="wheel_2_8_axis";
			boundary="wheel_2_8_bound";
			maxDroop=0;
			maxCompression=0;

		};

	};