	
	
	//Triggerd by BI eventhandler "Engine"

	_veh = _this select 0;
	_engineState = _this select 1;

	if (_engineState) then
	{
		
		_veh animateSource ['Indicator_Source', 1];

	};

	if (!_engineState) then
	{

		_veh animateSource ['Indicator_Source', 0];
		
	};