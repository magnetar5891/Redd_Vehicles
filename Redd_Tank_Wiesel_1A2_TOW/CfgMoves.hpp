

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Tank_Wiesel_1A2_TOW_Driver = "Redd_Tank_Wiesel_1A2_TOW_Driver";
            Redd_Tank_Wiesel_1A2_TOW_Driver_Out = "Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
            Redd_Tank_Wiesel_1A2_TOW_Commander = "Redd_Tank_Wiesel_1A2_TOW_Commander";
            Redd_Tank_Wiesel_1A2_TOW_Commander_Out = "Redd_Tank_Wiesel_1A2_TOW_Commander_Out";
            Redd_Tank_Wiesel_1A2_TOW_Commander_TOW = "Redd_Tank_Wiesel_1A2_TOW_Commander_TOW";
            Redd_Tank_Wiesel_1A2_TOW_Loader = "Redd_Tank_Wiesel_1A2_TOW_Loader";
            Redd_Tank_Wiesel_1A2_TOW_Loader_Out = "Redd_Tank_Wiesel_1A2_TOW_Loader_Out";
            Redd_Tank_Wiesel_1A2_TOW_Loader_MG = "Redd_Tank_Wiesel_1A2_TOW_Loader_MG";

            KIA_Redd_Tank_Wiesel_1A2_TOW_Driver = "KIA_Redd_Tank_Wiesel_1A2_TOW_Driver";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out = "KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Commander = "KIA_Redd_Tank_Wiesel_1A2_TOW_Commander";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out = "KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW = "KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Loader = "KIA_Redd_Tank_Wiesel_1A2_TOW_Loader";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out = "KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG = "KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG";

        };

    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            class Redd_Tank_Wiesel_1A2_TOW_Driver: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Driver: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A2_TOW_Driver_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Driver_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A2_TOW_Commander: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Commander.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Commander: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Commander.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A2_TOW_Commander_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Commander_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A2_TOW_Commander_TOW: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Commander_TOW.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Commander_TOW.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A2_TOW_Loader: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Loader.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Loader: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Loader.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A2_TOW_Loader_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Loader_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A2_TOW_Loader_MG: Crew
            {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\Redd_Tank_Wiesel_1A2_TOW_Loader_MG.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A2_TOW\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Loader_MG.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

        };

    };