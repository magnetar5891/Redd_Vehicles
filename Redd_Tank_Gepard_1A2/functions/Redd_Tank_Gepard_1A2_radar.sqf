

	params ["_veh"];

	waitUntil {!(alive _veh) or (isVehicleRadarOn _veh)};

	if !(alive _veh) exitWith {};

	_veh animateSource ["Radar_Rot_Source", 1];

	uiSleep 2;

	_veh animateSource ["Hide_Folgeradar_Inaktiv_Source", 1];
	_veh animateSource ["Hide_Suchradar_Inaktiv_Source", 1];

	uiSleep 0.9;

	_veh animateSource ["Hide_Folgeradar_Aktiv_Source", 0];
	_veh animateSource ["Hide_Suchradar_Aktiv_Source", 0];

	waitUntil {!(alive _veh) or !(isVehicleRadarOn _veh)};

	if !(alive _veh) exitWith {};

	_veh animateSource ["Hide_Folgeradar_Aktiv_Source", 1];
	_veh animateSource ["Hide_Suchradar_Aktiv_Source", 1];

	uiSleep 0.9;

	_veh animateSource ["Hide_Folgeradar_Inaktiv_Source", 0];
	_veh animateSource ["Hide_Suchradar_Inaktiv_Source", 0];
	
	_veh animateSource ["Radar_Rot_Source", 0];

	[_veh] spawn Redd_fnc_Gepard_Radar;