	///////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	Author: Redd
	//
	//	Description: 
	//				 
	//  Example: Triggerd by BI eventhandler "Engine"
	//			 		 
	//	Parameter(s): 0: OBJECT - Vehicle
	//				  1: BOOLEAN - Engine state
	//				  
	//	Returns: NA
	//  
	///////////////////////////////////////////////////////////////////////////////////////////////////	
	
	params ["_veh","_engineState"];
	
    //Engine On
	if (_engineState) then
	{
		
		_veh animateSource ['Indicator_Source', 1];

	};
    
    //Engine Off
	if (!_engineState) then
	{

		_veh animateSource ['Indicator_Source', 0];
		
	};