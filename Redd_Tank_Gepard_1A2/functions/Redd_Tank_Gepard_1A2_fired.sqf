

	//Triggered by BI eventhandler "fired" when Gepard fires a weapon

	params ["_veh","_weap"];
	
	waitUntil {!isNull _veh};
	
	if (_weap == "Redd_SmokeLauncher") then 
	{

		[_veh] spawn redd_fnc_SmokeLauncher; //Spawns smokelauncher function
		
	};