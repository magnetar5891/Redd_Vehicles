	
	
	class RenderTargets
	{
		
		//Driver left left mirror
		class Mirror1
		{

			renderTarget = "rendertarget0";
			
			class CameraView1
			{

				pointPosition		= "pip_left_left_pos";
				pointDirection		= "pip_left_left_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;	
						
			}; 	

		};
		
		//Driver left mirror and Co-driver left mirror
		class Mirror2
		{

			renderTarget = "rendertarget1";

			class CameraView1
			{

				pointPosition		= "pip_left_pos";
				pointDirection		= "pip_left_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Driver middle mirror and Co-driver middle mirror
		class Mirror3
		{

			renderTarget = "rendertarget2";

			class CameraView1
			{

				pointPosition		= "pip_middle_pos";
				pointDirection		= "pip_middle_dir";
				renderQuality 		= 1;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 

		};

		//Driver right mirror and Co-driver right mirror
		class Mirror4
		{

			renderTarget = "rendertarget3"; 
			
			class CameraView1
			{

				pointPosition		= "pip_right_pos";
				pointDirection		= "pip_right_dir";
				renderQuality 		= 1;			
				renderVisionMode 	= 0;			
				fov 				= 0.7;	
						
			}; 	

		};

		//Left mirror
		class Mirror5
		{

			renderTarget = "rendertarget4";

			class CameraView1
			{

				pointPosition		= "pip_left_mirror_pos";
				pointDirection		= "pip_left_mirror_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;	

			}; 

		};

		//Right mirror
		class Mirror6
		{

			renderTarget = "rendertarget5";

			class CameraView1
			{

				pointPosition		= "pip_right_mirror_pos";
				pointDirection		= "pip_right_mirror_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 	

		};

		//Right mirror2
		class Mirror7
		{

			renderTarget = "rendertarget6";

			class CameraView1
			{

				pointPosition		= "pip_right2_mirror_pos";
				pointDirection		= "pip_right2_mirror_dir";
				renderQuality 		= 2;
				renderVisionMode 	= 0;
				fov 				= 0.7;

			}; 	

		};
		
	};
