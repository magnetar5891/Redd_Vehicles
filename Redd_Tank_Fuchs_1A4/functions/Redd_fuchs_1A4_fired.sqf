

	//Triggered by BI eventhandler "fired" when Fuchs fires a weapon

	params ["_veh","_weap"];
	
	waitUntil {!isNull _veh};
	
	if (_weap == "Redd_SmokeLauncher") then 
	{

		[_veh] spawn redd_fnc_SmokeLauncher; //Spawns smokelauncher function
		
	};

	if (_weap == "Redd_Milan") then 
	{

		[_veh] call redd_fnc_Milan_Fired; //Calls function to drop empty Milan tube
		
	};