	
	
	//Triggerd by BI eventhandler "Engine"

	params ["_veh","_engineState"];

	if (_engineState) then
	{
		
		_veh animateSource ['Indicator_Source', 1];

	};

	if (!_engineState) then
	{

		_veh animateSource ['Indicator_Source', 0];
		
	};