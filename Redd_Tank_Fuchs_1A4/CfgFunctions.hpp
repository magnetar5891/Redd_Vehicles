
    
    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class Fuchs_Init
                {

                    file = "\Redd_Tank_Fuchs_1A4\functions\Redd_Fuchs_1A4_init.sqf";

                };

                class Fuchs_W_Init
                {

                    file = "\Redd_Tank_Fuchs_1A4\functions\Redd_Fuchs_1A4_W_init.sqf";

                };

                class Fuchs_GetOut
                {

                    file = "\Redd_Tank_Fuchs_1A4\functions\Redd_fuchs_1a4_GetOut.sqf";

                };

                class Fuchs_Plate
                {

                    file = "\Redd_Tank_Fuchs_1A4\functions\Redd_Fuchs_1A4_Plate.sqf";

                };

                class Fuchs_Bat_Komp
                {

                    file = "\Redd_Tank_Fuchs_1A4\functions\Redd_Fuchs_1A4_bat_komp.sqf";

                };

                class Fuchs_Fired
                {

                    file = "\Redd_Tank_Fuchs_1A4\functions\Redd_Fuchs_1A4_fired.sqf";

                };

                class Fuchs_Engine
                {

                    file = "\Redd_Tank_Fuchs_1A4\functions\Redd_Fuchs_1A4_Engine.sqf";

                };

            };

        };

    };