	
	
	//Basic parameters
	simulation = TankX;
	dampersBumpCoef = 0.3;
	terrainCoef = 0;

	//Fuel
	#define FUEL_FACTOR 0.165
	fuelCapacity = 200 * FUEL_FACTOR;
    ACE_refuel_fuelCapacity = 200;

	//Differential parameters
	//Nix

	//Engine parameters
	maxOmega = 628.32;
	minOmega = 104.72;
	enginePower = 63;
	peakTorque = 232;
	engineMOI = 3;
	idleRpm = 1000;
	redRpm = 6000;
	clutchStrength = 15;
	dampingRateFullThrottle = 0.25;
	dampingRateZeroThrottleClutchEngaged = 1;
	dampingRateZeroThrottleClutchDisengaged = 0.25;
	maxSpeed = 55;
	thrustDelay	= 0.05;
	brakeIdleSpeed = 2.8;
	normalSpeedForwardCoef = 0.60;
	slowSpeedForwardCoef = 0.30;
	
	tankTurnForce = 11*7000/2;
	tankTurnForceAngMinSpd = 0.4;
	tankTurnForceAngSpd = 0.4;
	
	accelAidForceCoef = 4;
	accelAidForceYOffset = -1;
	accelAidForceSpd = 13;
	
	torqueCurve[] = 
	{

		{"(0/6000)","(0/232)"},
		{"(857/6000)","(232/232)"},
		{"(1714/6000)","(232/232)"},
		{"(2571/6000)","(232/232)"},
		{"(3428/6000)","(174/232)"},
		{"(4285/6000)","(139/232)"},
		{"(5142/6000)","(116/232)"},
		{"(6000/6000)","(0/232)"}

	};

	//Floating and sinking
	waterPPInVehicle=0;
	maxFordingDepth=-0.75;
	waterResistance=0;
	canFloat = 0;
	waterLeakiness = 10;
	
	//Anti-roll bars
	antiRollbarForceCoef = 85;
	antiRollbarForceLimit = 85;
	antiRollbarSpeedMin = 0;
	antiRollbarSpeedMax	= 55;
	
	//Gearbox
	class complexGearbox 
	{
	
		GearboxRatios[] = {"R1", -6.5, "N", 0, "D1", 4, "D2", 3.5, "D3", 3.75};
		TransmissionRatios[] = {"High",6};
		gearBoxMode        = "auto";
		moveOffGear        = 1;
		driveString        = "D";
		neutralString      = "N";
		reverseString      = "R";

	};

	changeGearType="rpmratio";
	
	changeGearOmegaRatios[]=
	{

		(6000/6000),(1000/6000),
		(1000/6000),0,
		(2500/6000),(1500/6000),
		(4500/6000),(3000/6000),
		(6000/6000),(5000/6000)

	};

	switchTime = 0;
	latency = 1.5;
	engineLosses = 25;
	transmissionLosses = 15;

	//Wheel parameters
	driveOnComponent[] = {};
	wheelCircumference= 1.51;
	numberPhysicalWheels = 10;
	turnCoef = 5;

	class Wheels
	{	

		class L2
		{
			
			boneName = "wheel_podkoloL1";
			center   = "wheel_1_2_axis";
			boundary = "wheel_1_2_bound";
			steering = 0;
			side = "left";
			mass = 150;
			width = 0.22;
			MOI = 4;
			latStiffX = 25;
			latStiffY = 180;
			longitudinalStiffnessPerUnitGravity = 100000;
			maxBrakeTorque = 5000;
			sprungMass = 850;
			springStrength = 85000;
			springDamperRate = 25500;
			dampingRate = 1;
			dampingRateInAir = 280;
			dampingRateDamaged = 10;
			dampingRateDestroyed = 10000;
			maxCompression = 0.15;
			maxDroop = 0.15;
			frictionVsSlipGraph[]={{0,1},{0.35,0.95},{1,0.5}};

		};
		
		class L3: L2
		{
			
			boneName="wheel_podkolol2";
			center="wheel_1_3_axis";
			boundary="wheel_1_3_bound";
			
		};
		
		class L4: L2
		{
			
			boneName="wheel_podkolol3";
			center="wheel_1_4_axis";
			boundary="wheel_1_4_bound";
			
		};
		
		class L5: L2
		{
			
			boneName="wheel_podkolol4";
			center="wheel_1_5_axis";
			boundary="wheel_1_5_bound";
			
		};

		//Triebrad
		class L1: L2
		{
			
			boneName="";
			center="wheel_1_1_axis";
			boundary="wheel_1_1_bound";
			maxDroop=0;
			maxCompression=0;

		};
		
		class R2: L2
		{
			
			side="right";
			boneName="wheel_podkolop1";
			center="wheel_2_2_axis";
			boundary="wheel_2_2_bound";
			
		};
		
		class R3: R2
		{
			
			boneName="wheel_podkolop2";
			center="wheel_2_3_axis";
			boundary="wheel_2_3_bound";
			
		};

		class R4: R2
		{
			
			boneName="wheel_podkolop3";
			center="wheel_2_4_axis";
			boundary="wheel_2_4_bound";
			
		};
		
		class R5: R2
		{
			
			boneName="wheel_podkolop4";
			center="wheel_2_5_axis";
			boundary="wheel_2_5_bound";
			
		};
		
		//Triebrad
		class R1: R2
		{
			
			boneName="";
			center="wheel_2_1_axis";
			boundary="wheel_2_1_bound";
			maxDroop=0;
			maxCompression=0;
			
		};

	};