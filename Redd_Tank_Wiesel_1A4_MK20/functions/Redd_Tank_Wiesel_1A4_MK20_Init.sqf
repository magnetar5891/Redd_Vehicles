

	//Wiesel init

	_veh = _this select 0;

	waitUntil {!isNull _veh};

	[_veh,[[1],true]] remoteExecCall ['lockTurret']; //Locks commander high position

	[_veh] spawn redd_fnc_Wiesel_MK20_Plate;//spawns function to randomise license plates
	[_veh] spawn redd_fnc_Wiesel_MK20_Bat_Komp;//spawns function to set battalion and company numbers

	_veh setVariable ['Redd_Wiesel_Commander_Up', false,true];//initiates varibale for "climp up" and "climp down" function of turned out commander