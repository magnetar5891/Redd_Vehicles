

First of all, it is not perfect, but we are getting better ;)
Secondly, our vehicles are never meant to be played with AI, we are focusing on coop content. This means, some functions may not work with AI

Redd'n'Tank Vehicles 1.4.86
12.06.2018

The Team:
Redd: Config, functions, model, textures
Tank: Model, textures

Redd'n'Tank official website:
http://reddiem.portfoliobox.net/

Discuss AW-Forum (german): 
https://armaworld.de/index.php?thread/3258-redd-vehicles-marder-1a5-release/

Discuss BI-Forum (english):
https://forums.bistudio.com/forums/topic/210296-redd-vehicles-marder-1a5-release/

Steam Workshop:
http://steamcommunity.com/sharedfiles/filedetails/?id=1128145626

YouTube:
https://www.youtube.com/playlist?list=PLJVKqjrLClHTeQcHtncGIJdRyHiTblsdT

Imgur:
https://imgur.com/a/Uv1Ei

GitLab: (See the whole Changelog)
https://gitlab.com/TTTRedd/Redd_Vehicles
pls report issues here

Support us:
https://www.patreon.com/Redd_Arma_Modding

Contact:
redd.arma.modding@gmail.com

Addon Pack Contains:
- Gepard 1A2
- Wiesel 1A4 MK20 (No AI Vehicle)
- Wiesel 1A2 TOW (No AI Vehicle)
- Fuchs 1A4 Infantry (No AI Vehicle)
- Fuchs 1A4 Infantry-Milan (No AI Vehicle)
- Fuchs 1A4 Engineers (No AI Vehicle)
- Fuchs 1A4 Medic Vehicle (No AI Vehicle)
- Marder 1A5 (No AI Vehicle)
- Static Milan 
- Redd Vehicles Main

Features:
Gepard 1A2
- Full animated radar
- Commander and gunner have two stances when turned out
- Woodland, desert and winter camo
- Customisable Battalion/Regiment and Company numbers
Wiesel 1A4 MK20
- Woodland, desert and winter camo
- Commander has two stances when turned out
- ViV transportable
- Slingloadable
- Customisable Battalion and Company numbers
Wiesel 1A2 TOW
- Woodland, desert and winter camo
- ViV transportable
- Slingloadable
- Customisable Battalion and Company numbers
Fuchs 1A4 Medic Vehicle
- Actual Fuchs has two brake pedals, try the difference between normal brake (S) and handbrake (X). We tried to simulate pushing both pedals with the handbrake (X)
- Rotating blue light
- Woodland, desert and winter camo
- Animated doors
- Animated Surge Plate
- Animated Bullet Shield
- Customisable Battalion and Company numbers
Fuchs 1A4 Infantry, Infantry-Milan and Engineers
- FFV left and right rear hatch
- Actual Fuchs has two brake pedals, try the difference between normal brake (S) and handbrake (X). We tried to simulate pushing both pedals with the handbrake (X)
- Mountable rotating beacon
- Mountable Milan for middle hatch (Only Infantry-Milan)
- Woodland, desert and winter camo
- Animated doors
- Animated Surge Plate
- Animated Bullet Shield
- A few more little animations
- Customisable Battalion and Company numbers
Marder 1A5
- Rear ramp can be opened and closed by driver or rear FFV-seat
- Turned out Commander can assemble Milan on turret
- FFV, rear-back-seat, left-back-seat, right-back-seat
- Commander has two stances when turned out
- Marder has Static Milan in its cargo
- Woodland, desert and winter camo
- Customisable Battalion and Company numbers
Static Milan
- Static Milan can be assembled and disassembled

Known issues:
- Some missing details in Marder interior
- Localization only english and german, all other languages display english names
- Thermal vision always starts in black/white after changing the polarity once it has the correct color

Changelog:
Version 1.4.86
- Fixed some texture issues on Gepard 1A2
Version 1.4.85
- Added Gepard 1A2
- Added all wreck models, they can be found under "empty->wrecks->Redd'n'Tank Wrecks"
- Added empty Milan tube, can be found under "empty->things->objects"
- Readded static Milan
- Readded Milan to Marder and Fuchs
- Changed static Milan model and Marder Milan model to Tanks model
- Fixed "Fix GetIn Bug" user action radius was 25 meters, now its 5 meters
Version 1.3.79
- Added some Tank DLC config stuff
- Added distance LODs to Fuchs interior
- Added the "Fix GetIn Bug" user action, so you can now manually unlock the positions which sometimes wouldn't unlock and you cannot enter e.g. Fuchs co-driver anymore
- Added second Animation for Wiesel MK20 commander, now he can switch between a low and a high turned out animation (Low is default)
- Changed RSC UI optics for all vehicles and weapons
- Changed Fuchs "Commander Hatch" is now called "Middle Hatch" 
- Changed Marder and Wiesel MK20 turret rotating speed
- Changed new radio model in Fuchs interior
- Tweaked Marder, Wiesel TOW and Wiesel MK20 PhysX
- Tweaked all soundshader
- Tweaked some selection names and removed unnecessary config entries
- Removed Milan on Marder and Fuchs, due to a change in the engine it is not possible to aim with the missile if they are not part of the main turret (Will be fixed by BI)
- Removed static Milan due to an issue with the model.cfg for the backpacks
Version 1.3.66
- Fixed broken Main pbo BI Key
- Fixed broken static Milan backpacks
Version 1.3.64
- Fixed broken FFV positions
- Fixed Marder broken sandbags
Version 1.3.62
- Added Wiesel 1A4 MK20
- Added ACRE2 SEM90 Rack with two SEM70 to all vehicles
- Added support for virtual garage to all vehicles
- Added new optics for all weapons
- Changed all optics and separated day and night vision from thermal vision
- Changed MG3 250 rounds magazine to 120 rounds
- Changed Wiesel TOW now has 6 shots
- Changed Wiesel interior engine sound
- Changed reload animation for all Milan weapons
- Fixed Wiesel TOW can now be slingloaded again
- Cleaned all configs from unnecessary entries
Version 1.2.51
- Fixed Wiesel can now be dropped via parachute from ViV-transport
Version 1.2.50
- Added Wiesel 1A2 TOW
- Changed Milan tube backpack now carries the ammo for static milan
- Fixed MG3 soundshader
- Fixed MK20 soundshader
- Fixed Milan soundshader
- Fixed error in Fuchs stringtable.xml
- Fixed spelling mistake in Static Milan memorypoints "pos_gunner" and "pos_gunner_dir"
Version 1.1.44
- Added simple thermal texture to Fuchs 1A4
- Removed the function to force players to open doors and hatches manually before entering a vehicle for all vehicles. Seats are no longer locked. 
  User actions to open doors or hatches are still available, so you can decide by yourself if you want to open a door first or enter a vehicle vanilla style.
- Fixed Marder can now be refueled via ACE3
- Tweaked B.O. lights for all vehicles
- Tweaked some animations
Version 1.1.38
- Addes Fuchs 1A4 Infantry Group-Milan
- Addes Fuchs 1A4 Medic Vehicle
- Added a function to let players only enter vehicles if the doors/hatches are open (player has to open doors/hatches manually. Changing seats inside while doors are closed is no longer possible)
- Added Fuchs mountable rotating beacon (Commander can assemble and disassemble while turned out, Driver and Co-Driver can turn on and off)
- Added Marder "EngineMOI" PhysX parameter 
- Fixed, set launching positions for Marder smokegrenades on the correct position
- Removed stabilization for Marder maingun
- Changed, MG3 closeShot sound
- Changed german displayname "Beschussschild" -> "Beschussklappe"
- Changed position for Marder gunner to enter the vehicle to the rear ramp (rear ramp has to be open for entering)
- Tweaked Fuchs Gearbox so it will not stuck at 30 km/h at normal acceleration (W)
- Tweaked Marder Gearbox a bit
- Tweaked Marder engine parameters a bit
Version 1.1.25
- Added Fuchs 1A4 Infantry Group
- Added Fuchs 1A4 Engineer Group
- Added new PhysX parameters to Marder
- Added some MG3 mid and distance sounds from BW-Mod (Thanks to Ironie)
- Added 500 rounds MG3 magazines
- Fixed Marder tracks can now be repaired via ACE3
- Changed, Marder maingun is now stabilized in all axes
- Changed, Marder countermeasures moved to commander 
- Changed the spelling of all folders to lower case
- Changed MG3 tracercount
- Changed Redd_smokeLauncher.sqf so it will work for vehicles without turrets
- Changed SoundShaders for MG3
- Tweaked Marder engine parameters
Version 1.0.14
- Added new Redd'n'Tank logo
- Added Marder Drivewheel animation
- Fixed issue where choosing 2nd company shows the letter 1 
- Tweaked Marder Geo LOD, GeoPhysX LOD...again, tell me if it still flips
Version 1.0.10
- Added simple thermal texture to Marder 1A5
- Changed order for all optics to, "Normal" -> "NVG" -> "TI"
- Changed materials for glass
- Tweaked Geo LOD, GeoPhysX LOD and FireGeo LOD a bit
- Tweaked PhysX.hpp now it should drive much more smoother and flips less
Version 1.0.5
- Fixed issue where crew can get killed by small arms from outside of the vehicles, now it is possible to shoot or throw grenades through the rearramp
- Fixed missing modelpart
- Fixed missing track animation
Version 1.0.2
- Added strings for Marder 1A5 Eden attributes in stringtable.xml
- Fixed some commander animation issues

License:
Licensed Under Attribution-NonCommercial-NoDerivs CC BY-NC-ND
https://creativecommons.org/licenses/by-nc-nd/4.0/

Thanks:
- Thanks to T4nk for joining me
- Thanks to Tactical Training Team and TF47 for testing and support
- Thanks to Schubert for all background informations 
- Thanks to Pazuzu for original Fuchs config
- Thanks to commy2 for smoke launcher script
- Thanks to Ironie for MG3 sounds
- Thanks to Nobilis for help with support for virtual garage
- Thanks to Golani for promo shots
- Last but not least, thanks to my wife and my daughter for giving me the time to do all this stuff :* 