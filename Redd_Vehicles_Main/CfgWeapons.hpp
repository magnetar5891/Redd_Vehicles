	
	
	class Mode_SemiAuto;
	class Mode_Burst;
	class Mode_FullAuto;
	
	class CfgWeapons 
	{
		
		class autocannon_35mm;
		class LMG_coax;
		class missiles_titan;
		class SmokeLauncher;
		class Default;
		
		class Redd_MG3: LMG_coax
		{
			
			displayName = "MG 3";
			magazines[] = {"Redd_Mg3_Mag","Redd_Mg3_Mag_120"};
			magazineReloadTime = 8;
			autoReload = 1;
			ballisticsComputer = 2;
			modes[] = {"FullAuto","close","short","medium","far"};
			showToPlayer = 0;
			soundBurst = 0;
			
			class GunParticles 
			{

				class effect1 
				{
					
					positionName = "usti hlavne3";
					directionName = "konec hlavne3";
					effectName = "RifleAssaultCloud";
				};

				class effect2 
				{
					
					positionName = "machinegun_eject_pos";
					directionName = "machinegun_eject_dir";
					effectName = "Redd_MG3Cartridge";
					
				};
				
				class effect3
				{
					
					positionName = "machinegun_eject_pos";
					directionName = "machinegun_eject_dir";
					effectName = "Redd_MG3Eject";
					
				};
			
			};
			
			class FullAuto: Mode_FullAuto 
			{
				
				displayName = "MG 3";
				autoFire = 1;
				burst = 1;
				sounds[] = {"StandardSound"};
				reloadTime = (60/1200);
				dispersion = 0.0008;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_MG3_Shot_SoundSet", "Redd_MG3_Tail_SoundSet"};
					closure1[] = {"A3\sounds_f\weapons\closure\sfx7",0.56234133,1,40};
					closure2[] = {"A3\sounds_f\weapons\closure\sfx8",0.56234133,1,40};
					soundClosure[] = {"closure1",0.5,"closure2",0.5};
				};
				
			};
			
			class close: FullAuto
			{
				
				burst = 10;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.3;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 20;
				midRangeProbab = 0.7;
				maxRange = 50;
				maxRangeProbab = 0.1;
				showToPlayer = 0;
				
			};

			class short: close
			{
				
				burst = 8;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 300;
				minRange = 50;
				minRangeProbab = 0.05;
				midRange = 150;
				midRangeProbab = 0.7;
				maxRange = 300;
				maxRangeProbab = 0.1;
				
			};
			
			class medium: close
			{
				
				burst = 6;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 600;
				minRange = 200;
				minRangeProbab = 0.05;
				midRange = 400;
				midRangeProbab = 0.7;
				maxRange = 600;
				maxRangeProbab = 0.1;
				
			};
			
			class far: close
			{
				
				burst = 4;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 1000;
				minRange = 400;
				minRangeProbab = 0.05;
				midRange = 750;
				midRangeProbab = 0.4;
				maxRange = 1200;
				maxRangeProbab = 0.01;
				
			};
			
		};
		
		class Redd_MK20: autocannon_35mm
		{
			scope = 1;
			displayName = "MK 20mm";
			magazines[] = {"Redd_MK20_AP_Mag","Redd_MK20_HE_Mag"};
			muzzles[] = {"this"};
			multiplier = 1;
			magazineReloadTime = 0.1;
			ballisticsComputer = 2;
			autoReload = 1;
			autoFire = 0;
			soundContinuous = 0;			
			burst = 1;
			modes[] = {"Single","FullAuto","close","short","medium","far"};
			showToPlayer = 0;
			soundBurst = 0;
			
			class gunParticles 
			{
				
				class effect1 
				{
					
					positionName = "usti hlavne";
					directionName = "konec hlavne";
					effectName = "Redd_AutoCannonFired";
					
				};
				
				class effect2
				{
					
					positionName = "shell_eject_pos";
					directionName = "shell_eject_dir";
					effectName = "Redd_HeavyGunCartridge";
					
				};
				
				class effect3
				{
					
					positionName = "shell_eject_pos";
					directionName = "shell_eject_dir";
					effectName = "Redd_HeavyGunEject";
					
				};
				
			};
			
			class HE: autocannon_35mm {
			};

			class AP: autocannon_35mm {
			};
			
			class FullAuto: Mode_FullAuto 
			{
				
				displayName = "MK 20mm";
				autoFire = 1;
				sounds[] = {"StandardSound"};
				reloadTime = (60/900);
				dispersion = 0.0008;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_Mk20_Shot_SoundSet", "Redd_Mk20_Tail_SoundSet"};
				
				};
				
			};
			
			class Single: Mode_SemiAuto 
			{
				
				displayName = "MK 20mm";
				autoFire = 0;
				sounds[] = {"StandardSound"};
				reloadTime = (60/900);
				dispersion = 0.0008;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_Mk20_Shot_SoundSet", "Redd_Mk20_Tail_SoundSet"};
					
				};
				
			};
			
			class close: FullAuto
			{
				
				burst = 10;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.3;
				aiRateOfFireDistance = 500;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 250;
				midRangeProbab = 0.7;
				maxRange = 500;
				maxRangeProbab = 0.1;
				showToPlayer = 0;
				
			};
			
			class short: close
			{
				
				burst = 7;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 1000;
				minRange = 500;
				minRangeProbab = 0.05;
				midRange = 750;
				midRangeProbab = 0.7;
				maxRange = 1000;
				maxRangeProbab = 0.1;
				
			};
			
			class medium: close
			{
				
				burst = 4;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 1500;
				minRange = 100;
				minRangeProbab = 0.05;
				midRange = 1200;
				midRangeProbab = 0.7;
				maxRange = 1500;
				maxRangeProbab = 0.1;
				
			};
			
			class far: close
			{
				
				burst = 1;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 2000;
				minRange = 1500;
				minRangeProbab = 0.05;
				midRange = 1750;
				midRangeProbab = 0.4;
				maxRange = 2000;
				maxRangeProbab = 0.01;
				
			};
			
		};

		class Redd_MK20FL: Redd_MK20
		{
			
			magazines[] = {"Redd_MK20_AP_Mag120","Redd_MK20_HE_Mag200"};
			ballisticsComputer = 16;

		};

		class Redd_35mm: autocannon_35mm
		{

			scope = 1;
			displayName = "35mm KDA L/90";
			magazines[] = {"Redd_35mm_HE_Mag","Redd_35mm_AP_Mag"};
			muzzles[] = {"this"};
			multiplier = 1;
			magazineReloadTime = 0.1;
			ballisticsComputer = 1;
			autoReload = 1;
			autoFire = 0;
			soundContinuous = 0;
			modes[] = {"Single","Burst","FullAuto","close","short","medium","far"};
			showToPlayer = 0;
			soundBurst = 0;
			shotFromTurret=0;
			textureType="burst";

			class gunParticles 
			{
				
				class effect1 
				{
					
					positionName = "usti hlavne 1";
					directionName = "konec hlavne 1";
					effectName = "Redd_AutoCannonFired";
					
				};

				class effect2 
				{
					
					positionName = "usti hlavne 2";
					directionName = "konec hlavne 2";
					effectName = "Redd_AutoCannonFired";
					
				};

				class effect3 
				{
					
					positionName = "usti hlavne 4";
					directionName = "konec hlavne 4";
					effectName = "Redd_AutoCannonFired";
					
				};

				class effect4 
				{
					
					positionName = "usti hlavne 5";
					directionName = "konec hlavne 5";
					effectName = "Redd_AutoCannonFired";
					
				};
				
				class effect5
				{
					
					positionName = "shell_eject_pos 1";
					directionName = "shell_eject_dir 1";
					effectName = "Redd_HeavyGunCartridge_2";
					
				};

				class effect6
				{
					
					positionName = "shell_eject_pos 2";
					directionName = "shell_eject_dir 2";
					effectName = "Redd_HeavyGunCartridge_2";
					
				};
				
				class effect7
				{
					
					positionName = "gurt_eject_pos 1";
					directionName = "gurt_eject_dir 1";
					effectName = "Redd_HeavyGunEject_2";
					
				};

				class effect8
				{
					
					positionName = "gurt_eject_pos 2";
					directionName = "gurt_eject_dir 2";
					effectName = "Redd_HeavyGunEject_2";
					
				};
				
			};
			
			class HE: autocannon_35mm {
			};

			class AP: autocannon_35mm {
			};
			
			class FullAuto: Mode_FullAuto 
			{
				
				displayName = "35mm KDA L/90";
				autoFire = 1;
				sounds[] = {"StandardSound"};
				reloadTime = (60/1100);
				dispersion = 0.002;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_35mm_Shot_SoundSet", "Redd_35mm_Tail_SoundSet"};
				
				};
				
			};
			
			class Burst: Mode_Burst 
			{
				
				displayName = "35mm KDA L/90";
				autoFire = 0;
				sounds[] = {"StandardSound"};
				reloadTime = (60/1100);
				dispersion = 0.002;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				burst = 10;
				
				class StandardSound
				{
					
					soundsetshot[] = {"Redd_35mm_Shot_SoundSet", "Redd_35mm_Tail_SoundSet"};
					
				};
				
			};
			
			class Single: Mode_SemiAuto 
			{
				
				displayName = "35mm KDA L/90";
				autoFire = 0;
				sounds[] = {"StandardSound"};
				reloadTime = (60/1100);
				dispersion = 0.002;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 50;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 0;
				midRangeProbab = 0.7;
				maxRange = 0;
				maxRangeProbab = 0.1;
				showToPlayer = 1;
				burst = 1;

				class StandardSound
				{
					
					soundsetshot[] = {"Redd_35mm_Shot_SoundSet", "Redd_35mm_Tail_SoundSet"};
					
				};
				
			};

			class close: FullAuto
			{
				
				burst = 18;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.3;
				aiRateOfFireDistance = 750;
				minRange = 0;
				minRangeProbab = 0.05;
				midRange = 500;
				midRangeProbab = 0.7;
				maxRange = 1000;
				maxRangeProbab = 0.1;
				showToPlayer = 0;
				
			};
			
			class short: close
			{
				
				burst = 15;
				aiBurstTerminable = 1;
				aiRateOfFire = 0.5;
				aiRateOfFireDistance = 1250;
				minRange = 750;
				minRangeProbab = 0.05;
				midRange = 1250;
				midRangeProbab = 0.7;
				maxRange = 1500;
				maxRangeProbab = 0.1;
				
			};
			
			class medium: close
			{
				
				burst = 12;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 1750;
				minRange = 1250;
				minRangeProbab = 0.05;
				midRange = 1750;
				midRangeProbab = 0.7;
				maxRange = 2000;
				maxRangeProbab = 0.1;
				
			};
			
			class far: close
			{
				
				burst = 9;
				aiBurstTerminable = 1;
				aiRateOfFire = 1;
				aiRateOfFireDistance = 2250;
				minRange = 1750;
				minRangeProbab = 0.05;
				midRange = 3000;
				midRangeProbab = 0.4;
				maxRange = 3500;
				maxRangeProbab = 0.01;
				
			};

		};
		
		class Redd_Gesichert: Default 
		{
			
			scope = 2;
			displayName = "$STR_Redd_Gesichert";
			magazines[] = {};
			textureType = "fullAuto";
			
		};
		
		class Redd_Milan: missiles_titan
		{
			
			displayName = "Milan";
			magazines[] = {"Redd_Milan_Mag"};
			ballisticsComputer = 0;
			autoReload = 1;
			magazineReloadTime = 15;
			reloadTime = 1;
			minRange = 150;
			minRangeProbab = 0.600000;
			midRange = 1500;
			midRangeProbab = 0.700000;
			maxRange = 4000;
			maxRangeProbab = 0.001000;
			canLock = 0;

			class StandardSound 
			{
			
				soundSetShot[] = {"Redd_Milan_Shot_SoundSet", "Redd_Milan_Tail_SoundSet"};

			};
			
		};

		class Redd_TOW: Redd_Milan
		{

			displayName = "TOW";
			magazines[] = {"Redd_TOW_Mag"};
			
		};
		
		class Redd_SmokeLauncher: SmokeLauncher 
		{
			
			magazines[] = {"Redd_SmokeLauncherMag"};
			reloadTime = 3;
			magazineReloadTime = 30;
			autoFire = 0;
			simulation = "cmlauncher";
			showToPlayer = 1;
			minRange = 0;
			maxRange = 10000;
			textureType = "semi";
			canLock = 0;
			
		};
		
	};