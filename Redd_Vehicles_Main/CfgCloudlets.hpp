	
	
	class CfgCloudlets 
	{
		class MachineGunCartridge2;
		class MachineGunEject;
		class AutoCannonFired;

		class Redd_AutoCannonFired: AutoCannonFired
		{

			color[]=
			{

				{0.059999999,0.059999999,0.059999999,0.039999999},
				{0.30000001,0.30000001,0.30000001,0.050000001},
				{0.30000001,0.30000001,0.30000001,0.0049999999},
				{0.30000001,0.30000001,0.30000001,0.001}
				
			};

		};
		
		class Redd_HeavyGunCartridge: MachineGunCartridge2 
		{
			
			moveVelocity[] = {"-directionX * 5","- directionY * 5","- directionZ * 5"};
			size[] = {2.5};
			sizeVar = 0;
			bounceOnSurface = 0.3;
			bounceOnSurfaceVar = 0.2;
			lifeTimeVar = 0;
			angleVar = 0.5;
			rotationVelocity = 1;
			randomDirectionPeriod = 0.1;
			randomDirectionIntensity = 0.05;
			MoveVelocityVar[] = {0.2,0.2,0.2};
			rotationVelocityVar = 2;
			positionVar[] = {0.05,0.05,0.05};
			positionVarConst[] = {0,0,0};
			lifeTime = 10;
			
		};

		class Redd_HeavyGunCartridge_2: Redd_HeavyGunCartridge 
		{

			size[] = {3.5};

		};		
		
		class Redd_HeavyGunEject: MachineGunEject
		{
			
			moveVelocity[] = {"-directionX * 5","- directionY * 5","- directionZ * 5"};
			size[] = {2};
			sizeVar = 0;
			bounceOnSurface = 0.3;
			bounceOnSurfaceVar = 0.2;
			lifeTimeVar = 0;
			angleVar = 0.5;
			rotationVelocity = 1;
			randomDirectionPeriod = 0.1;
			randomDirectionIntensity = 0.05;
			MoveVelocityVar[] = {0.2,0.2,0.2};
			rotationVelocityVar = 2;
			positionVar[] = {0.05,0.05,0.05};
			positionVarConst[] = {0,0,0};
			lifeTime = 10;
			
		};

		class Redd_HeavyGunEject_2: Redd_HeavyGunEject
		{

			size[] = {3};

		};

		class Redd_MG3Cartridge: MachineGunCartridge2 
		{
			
			moveVelocity[] = {"-directionX * 3","- directionY * 3","- directionZ * 3"};
			size[] = {1.2};
			sizeVar = 0;
			bounceOnSurface = 0.3;
			bounceOnSurfaceVar = 0.2;
			lifeTimeVar = 0;
			angleVar = 0.5;
			rotationVelocity = 1;
			randomDirectionPeriod = 0.1;
			randomDirectionIntensity = 0.05;
			MoveVelocityVar[] = {0.15,0.15,0.15};
			rotationVelocityVar = 2;
			positionVar[] = {0.05,0.05,0.05};
			positionVarConst[] = {0,0,0};
			lifeTime = 10;
			
		};
		
		class Redd_MG3Eject: MachineGunEject
		{
			
			moveVelocity[] = {"-directionX * 3","- directionY * 3","- directionZ * 3"};
			size[] = {1.2};
			sizeVar = 0;
			bounceOnSurface = 0.3;
			bounceOnSurfaceVar = 0.2;
			lifeTimeVar = 0;
			angleVar = 0.5;
			rotationVelocity = 1;
			randomDirectionPeriod = 0.1;
			randomDirectionIntensity = 0.05;
			MoveVelocityVar[] = {0.15,0.15,0.15};
			rotationVelocityVar = 2;
			positionVar[] = {0.05,0.05,0.05};
			positionVarConst[] = {0,0,0};
			lifeTime = 10;
			
		};

	};

	class Redd_AutoCannonFired
	{
		
		class Redd_AutoCannonFired
		{

			simulation="particles";
			type="Redd_AutoCannonFired";
			position[]={0,0,0};
			intensity=1;
			interval=1;
			lifeTime=0.050000001;

		};

	};
	
	class Redd_HeavyGunCartridge
	{
		
		class Redd_HeavyGunCartridge
		{
			
			simulation = "particles";
			type = "Redd_HeavyGunCartridge";
			position[] = {0,0,0};
			intensity = 1;
			interval = 1;
			lifeTime = 0.05;
			qualityLevel = -1;
			
		};
		
	};

	class Redd_HeavyGunCartridge_2
	{
		
		class Redd_HeavyGunCartridge_2
		{
			
			simulation = "particles";
			type = "Redd_HeavyGunCartridge_2";
			position[] = {0,0,0};
			intensity = 1;
			interval = 1;
			lifeTime = 0.05;
			qualityLevel = -1;
			
		};
		
	};
	
	class Redd_HeavyGunEject
	{
		
		class Redd_HeavyGunEject
		{
			
			simulation = "particles";
			type = "Redd_HeavyGunEject";
			position[] = {0,0,0};
			intensity = 1;
			interval = 1;
			lifeTime = 0.05;
			qualityLevel = -1;
			
		};
		
	};

	class Redd_HeavyGunEject_2
	{
		
		class Redd_HeavyGunEject_2
		{
			
			simulation = "particles";
			type = "Redd_HeavyGunEject_2";
			position[] = {0,0,0};
			intensity = 1;
			interval = 1;
			lifeTime = 0.05;
			qualityLevel = -1;
			
		};
		
	};

	class Redd_MG3Cartridge
	{
		
		class Redd_MG3Cartridge
		{
			
			simulation = "particles";
			type = "Redd_MG3Cartridge";
			position[] = {0,0,0};
			intensity = 1;
			interval = 1;
			lifeTime = 0.05;
			qualityLevel = -1;
			
		};
		
	};
	
	class Redd_MG3Eject
	{
		
		class Redd_MG3Eject
		{
			
			simulation = "particles";
			type = "Redd_MG3Eject";
			position[] = {0,0,0};
			intensity = 1;
			interval = 1;
			lifeTime = 0.05;
			qualityLevel = -1;
			
		};
		
	};