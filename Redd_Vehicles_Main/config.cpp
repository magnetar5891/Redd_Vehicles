	
	
	#include "CfgPatches.hpp"
	#include "CfgEditorClass.hpp"
	#include "CfgCloudlets.hpp"
	#include "CfgMagazines.hpp"
	#include "CfgWeapons.hpp"
	#include "CfgSounds.hpp"
	#include "CfgSoundset.hpp"
	#include "CfgSoundshaders.hpp"
	#include "CfgAnimationSourceSounds.hpp"
	#include "CfgFunctions.hpp"
	#include "CfgInsignia.hpp"
	#include "CfgRscInGameUI.hpp"
	
	class CfgVehicles 
	{

		class thingX;
		class Wreck_Base_F;

		class Redd_Milan_Rohr: thingX
		{
		
			displayName = "$STR_Milan_Rohr_Leer";
			model = "\Redd_Vehicles_Main\data\Redd_Milan_Rohr.p3d";
			scope = 2;
			scopeCurator = 2;
			
		};

		class Redd_Wrecks_Main: Wreck_Base_F
		{

			icon = "iconObject_1x1";
			editorSubcategory = "Redd_Wrecks";
			scope = 1;
			scopeCurator = 1;

		};

		class Redd_Wiesel_TOW_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "Wiesel_TOW_Wreck";
			model = "\Redd_Tank_Wiesel_1A2_TOW\Redd_Tank_W_1A2_TOW_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			
		};

		class Redd_Wiesel_MK20_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "Wiesel_MK20_Wreck";
			model = "\Redd_Tank_Wiesel_1A4_MK20\Redd_Tank_W_1A4_MK20_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			
		};

		class Redd_Fuchs_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "Fuchs_Wreck";
			model = "\Redd_Tank_Fuchs_1A4\Redd_Tank_Fuchs_1A4_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			
		};

		class Redd_Marder_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "Marder_Wreck";
			model = "\Redd_Marder_1A5\Redd_Marder_1A5_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			
		};

		class Redd_Gepard_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "Gepard_Wreck";
			model = "\Redd_Tank_Gepard_1A2\Redd_Tank_Gepard_1A2_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			
		};

	};